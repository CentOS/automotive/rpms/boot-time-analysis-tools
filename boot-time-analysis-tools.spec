%global debug_package %{nil}

Name:               boot-time-analysis-tools
Version:            0.5
Release:            1%{?dist}
Summary:            Collection of tools to analyse boot time.

License:            LGPLv2
URL:                https://gitlab.com/CentOS/automotive/src/boot-time-analysis-tools.git

Source:             https://gitlab.com/CentOS/automotive/src/boot-time-analysis-tools/-/archive/0.5/boot-time-analysis-tools-0.5.tar.gz

BuildRequires:      python3-devel pyproject-rpm-macros
BuildRequires:      meson gcc systemd-rpm-macros
%{?systemd_requires}

%description
The boot_timings script collects boot time log information from dbus, dmesg,
and systemd.
The normalized logs are also fed into the `boot_log_chart.py` script, which
creates an HTML/JavaScript interactive cart output as a `_chart.html` file.


%prep
%setup -T -b 0 -q


%generate_buildrequires
%pyproject_buildrequires


%build
%pyproject_wheel
%ifarch aarch64
cd cntvct-log
%meson
%meson_build
%endif


%install
%pyproject_install
%pyproject_save_files '*'
%ifarch aarch64
cd cntvct-log
%meson_install
install -DpZm 0644 usr/lib/systemd/system/cntvct@.service %{buildroot}%{_unitdir}/cntvct@.service
install -DpZm 0755 usr/lib/dracut/modules.d/90cntvct/module-setup.sh %{buildroot}%{_prefix}/lib/dracut/modules.d/90cntvct/module-setup.sh
%endif


%files -f %{pyproject_files}
%doc README.md
%{_bindir}/boot_timings
%ifarch aarch64
%{_bindir}/cntvct
%{_unitdir}/cntvct@.service
%{_prefix}/lib/dracut/modules.d/90cntvct/module-setup.sh
%endif


%ifarch aarch64
%post
%systemd_post cntvct@.service
%endif

%ifarch aarch64
%preun
%systemd_preun cntvct@.service
%endif

%ifarch aarch64
%postun
%systemd_postun_with_restart cntvct@.service
%endif


%changelog
* Fri Jan 17 2025 Eric Chanudet <echanude@redhat.com> 0.5
- setup.py: v0.5
- feat: add option to specify offset
* Thu Sep 12 2024 Eric Chanudet <echanude@redhat.com> 0.4
- cntvct: innocuous typo in dracut script
- cntvct: remove SPDX and inherit project license
- Add LICENSE
- README: amend docs for package
- cntvct-log: amend typo in README
- correct offset messages and improve line spacing of print output
* Wed May 15 2024 Eric Chanudet <echanude@redhat.com> 0.2
- Package the python scripts relying on macros and setuptools.
- Make cntvct build optional and only build when targetting aarch64.
- Remove sub packages.
* Fri Mar 15 2024 Eric Chanudet <echanude@redhat.com> 0.1
- cntvct-log, a simple tool to log the arch counter at various targets. This is
  only provided for aarch64, as it is architecture specific.
